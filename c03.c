#include <stdio.h>
int main(){
	int num1,num2,temp;
	printf("\nEnter 1st number : ");
	scanf("%d",&num1);
	printf("\nEnter 2nd number : ");
	scanf("%d",&num2);
	temp = num1;
	num1 = num2;
	num2 = temp;
	printf("\n1st number after swap : %d",num1);
	printf("\n2nd number after swap : %d",num2);
	return 0;
}
